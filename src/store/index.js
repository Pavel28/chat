import Vue from "vue"
import Vuex from "vuex"
import firebase from "firebase/app"
import colors from "color-generator"
import "firebase/auth"
import "firebase/database"

Vue.use(Vuex)

function initState() {
    return {
        user: {
            email: "",
            emailColor: "",
            uid: null
        },
        userPromise: null,
        messages: {},
        messageColor: {}
    }
}

export default new Vuex.Store({
    state: initState(),
    mutations: {
        SET_USER(state, user) {
            state.user = {
                email: user.email,
                uid: user.uid
            }
        },
        SET_USER_PROMISE(state, userPromise) {
            state.userPromise = userPromise
        },
        RESET(state) {
            Object.assign(state, initState())
        },
        UPDATE_MESSAGES(state, messages) {
            state.messages = Object.assign({}, state.messages, messages)

            for (let id in state.messages) {
                if (state.messageColor[state.messages[id].email] === undefined) state.messageColor[state.messages[id].email] = colors(1, 0.5).hexString()
            }
        }
    },
    actions: {
        isAuth({ state, commit }) {
            console.log("isAuth")

            if (state.user.uid) return Promise.resolve(true)
            else if (state.userPromise) return state.userPromise
            else {
                console.log("isAuth create promise")
                const userPromise = new Promise((resolve, reject) => {
                    firebase.auth().onAuthStateChanged(
                        (user) => {
                            console.log("is auth", user)
                            if (user) {
                                commit("SET_USER", user)
                                resolve(true)
                            } else resolve(false)
                        },
                        (error) => {
                            reject(error)
                        }
                    )
                })
                commit("SET_USER_PROMISE", userPromise)
                return userPromise
            }
        },
        login({ commit }, { email, password }) {
            return firebase
                .auth()
                .signInWithEmailAndPassword(email, password)
                .then((response) => {
                    commit("SET_USER", response.user)
                    return Promise.resolve()
                })
                .catch((error) => {
                    return Promise.reject(error.message)
                })
        },
        register({ commit }, { email, password }) {
            return firebase
                .auth()
                .createUserWithEmailAndPassword(email, password)
                .then((response) => {
                    commit("SET_USER", response.user)
                    return Promise.resolve()
                })
                .catch((error) => {
                    return Promise.reject(error.message)
                })
        },
        logout({ commit }) {
            return firebase
                .auth()
                .signOut()
                .finally(() => {
                    commit("RESET")
                })
        },
        sendMessage({ state }, message) {
            firebase
                .database()
                .ref("messages")
                .push({
                    message: message,
                    email: state.user.email
                })
        },
        updateMessages({ commit }) {
            firebase
                .database()
                .ref("messages/")
                .on("value", (snapshot) => {
                    commit("UPDATE_MESSAGES", snapshot.val())
                })
        }
    },
    getters: {}
})
