import Vue from "vue"
import VueRouter from "vue-router"
import store from "../store"

Vue.use(VueRouter)

const ifAccessed = (to, from, next) => {
    store
        .dispatch("isAuth")
        .then((status) => {
            console.log(status)
            if (status && to.meta.access === "guest") next({ name: "Home" })
            else if (!status && to.meta.access === "auth") next({ name: "Login" })
            else next()
        })
        .catch(() => {
            next({ name: "Login" })
        })
}

const routes = [
    {
        path: "/",
        name: "Home",
        meta: { access: "auth" },
        beforeEnter: ifAccessed,
        components: {
            header: () => import("../components/Header"),
            default: () => import("../views/Home"),
            footer: () => import("../components/Footer")
        }
    },
    {
        path: "/login",
        name: "Login",
        beforeEnter: ifAccessed,
        meta: { access: "guest" },
        components: {
            header: () => import("../components/AuthHeader"),
            default: () => import("../views/Login")
        }
    },
    {
        path: "/registration",
        name: "Registration",
        beforeEnter: ifAccessed,
        meta: { access: "guest" },
        components: {
            header: () => import("../components/AuthHeader"),
            default: () => import("../views/Registration")
        }
    },
    {
        path: "/*",
        component: () => import("../views/NotFound")
    }
]

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
})

export default router
