import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import firebase from "firebase/app"
import Vuelidate from "vuelidate"
import Gravatar from "vue-gravatar"
import "./quasar"
import "./registerServiceWorker"

Vue.use(Vuelidate)
Vue.component("v-gravatar", Gravatar)

Vue.config.productionTip = false

firebase.initializeApp({
    apiKey: "AIzaSyDUtQwAGU6OVjyHqDMaZ8yKObrrtHvRoPQ",
    authDomain: "chat-962e8.firebaseapp.com",
    databaseURL: "https://chat-962e8.firebaseio.com",
    projectId: "chat-962e8",
    storageBucket: "chat-962e8.appspot.com",
    messagingSenderId: "2537089007",
    appId: "1:2537089007:web:8957871cf496010bd4b81a"
})

new Vue({
    router,
    store,
    render: (h) => h(App)
}).$mount("#app")
