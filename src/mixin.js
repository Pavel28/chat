export const showNotifyMixin = {
    methods: {
        showNotify(message, type) {
            this.$q.notify({ type, message })
        }
    }
}
